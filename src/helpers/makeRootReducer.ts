import {combineReducers, Reducer, ReducersMapObject} from "redux";
import {Reducer as _Reducer} from "../classes";

export type StateDefinition<T> = {[P in keyof T]: typeof _Reducer | Reducer<T[P]> | StateDefinition<T[P]>};

export function makeRootReducer<StateTree>(stateDefinition: StateDefinition<StateTree>): Reducer<StateTree> {
    const dst: ReducersMapObject = {};

    function makeReducer(stateDefinition, dst) {
        Object.keys(stateDefinition)
            .forEach(key => {
                if (typeof stateDefinition[key] === "function") {
                    dst[key] = stateDefinition[key].prototype instanceof _Reducer
                        ? (stateDefinition[key] as typeof _Reducer).createReducer()
                        : stateDefinition[key];
                } else if (typeof stateDefinition[key] === "object") {
                    dst[key] = combineReducers(makeReducer(stateDefinition[key], dst[key] = dst[key] || {}));
                }
            });

        return dst;
    }

    return combineReducers<StateTree>(makeReducer(stateDefinition, dst));
}

export default makeRootReducer;