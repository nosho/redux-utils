import * as React from "react";
import {Provider as ReactReduxProvider} from "react-redux";
import {applyMiddleware, createStore, Middleware, Store, StoreEnhancer} from "redux";
import {ActionCreator} from "../classes";
import {makeRootReducer, StateDefinition} from "../helpers";

export interface StateDefinitionProviderProps<StateTree> {
    stateDefinition: StateDefinition<StateTree>;
    middleware?: Middleware[];
    enhancer?: StoreEnhancer<StateTree>;
}

export interface StateDefinitionProviderState<StateTree> {
    store?: Store<StateTree>;
}

export class StateDefinitionProvider<Props extends StateDefinitionProviderProps<StateTree>, State extends StateDefinitionProviderState<StateTree>, StateTree> extends React.Component<Props, State> {

    protected componentWillMount(): void {
        this.setState({
            store: createStore<StateTree>(
                makeRootReducer<StateTree>(this.props.stateDefinition),
                this.props.enhancer || require("redux-devtools-extension").composeWithDevTools(
                    applyMiddleware(
                        ...ActionCreator.middleware,
                        ...(this.props.middleware || [])
                    )
                )
            )
        });
    }

    public render(): JSX.Element {
        const childCount = React.Children.count(this.props.children);
        let el;

        if (!childCount) {
            return null;
        } else if (childCount === 1) {
            el = React.Children.only(this.props.children);
        } else {
            el = <div className="state-definition-provider">{this.props.children}</div>;
        }

        // set name for more readable component tree
        (ReactReduxProvider as any).displayName = "ReactReduxProvider";

        return <ReactReduxProvider store={this.state.store}>{el}</ReactReduxProvider>;
    }

}

export default StateDefinitionProvider;