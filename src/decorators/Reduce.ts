import {Action, Reducer} from "../classes";

export const ReduceMetadataKey: string = "Reducer.reducers";
export const ReduceMetadataProperty: string = "reducers";

export function Reduce(...actionTypes: (string | typeof Action)[]): MethodDecorator {
    const _actionTypeStrings: string[] = actionTypes.map(actionType => typeof actionType === "function"
        ? actionType.type
        : actionType
    );

    return (target: Reducer<any>, methodName: string) => {
        Reflect.defineMetadata(ReduceMetadataKey, [
            ...(Reflect.getMetadata(ReduceMetadataKey, target, ReduceMetadataProperty) || []),
            {methodName, actionTypes: _actionTypeStrings}
        ], target, ReduceMetadataProperty);
    };
}

export default Reduce;