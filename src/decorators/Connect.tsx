import * as React from "react";
import {connect, Dispatch, MapDispatchToProps, MapStateToProps} from "react-redux";
import {ActionCreator} from "../classes";

export interface ActionCreatorMap {
    [name: string]: ActionCreatorMap | typeof ActionCreator;
}

export function Connect<TStateProps, TDispatchProps, TOwnProps>(mapStateToProps?: MapStateToProps<TStateProps, TOwnProps>,
                                                                actionCreatorMap?: ActionCreatorMap): ClassDecorator {
    return (target: React.ComponentClass<any>): React.ComponentClass<any> => {
        const ConnectedComponent = connect<TStateProps, TDispatchProps, TOwnProps>(
            mapStateToProps,
            (actionCreatorMap && transformActionCreatorMapToDispatchToPropsMap(actionCreatorMap))
        )(target);

        class WrappedComponent extends target {

            public render() {
                return <ConnectedComponent {...this.props} />;
            }

        }

        Object.assign(WrappedComponent, target);

        Object.defineProperty(WrappedComponent, "name", {value: `@Connect(...)(${target.name})`});

        (WrappedComponent as any).propTypes = target.propTypes;
        (WrappedComponent as any).contextTypes = target.contextTypes;
        (WrappedComponent as any).childContextTypes = target.childContextTypes;
        (WrappedComponent as any).defaultProps = target.defaultProps;
        (WrappedComponent as any).displayName = target.displayName;
        (WrappedComponent as any).WrappedComponent = target;

        return WrappedComponent;
    }
}

function transformActionCreatorMapToDispatchToPropsMap(actionCreatorMap: ActionCreatorMap): MapDispatchToProps<any, any> {
    function transform(dispatch: Dispatch<any>, _actionCreatorMap: ActionCreatorMap = actionCreatorMap, props: any = {}) {
        Object.keys(_actionCreatorMap)
            .forEach(key => {
                const value = _actionCreatorMap[key];

                if ((value as typeof ActionCreator).prototype instanceof ActionCreator) {
                    props[key] = (value as typeof ActionCreator).bindToDispatch(dispatch);
                } else if (typeof value === "object") {
                    props[key] = transform(dispatch, value, props[key]);
                }
            });

        return props;
    }

    return (dispatch: Dispatch<any>) => transform(dispatch);
}

export default Connect;