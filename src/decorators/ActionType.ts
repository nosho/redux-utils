import "reflect-metadata";
import {Action} from "../classes/Action";

export const ActionTypeMetadataKey = "actionType";

export function ActionType<T>(actionType: T): ClassDecorator {
    return (target: new() => Action<T, any>) => {
        Reflect.defineMetadata(ActionTypeMetadataKey, actionType, target);
    }
}

export default ActionType;