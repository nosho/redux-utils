import {Action as _Action} from "redux";
import {ActionTypeMetadataKey} from "../decorators/ActionType";

export interface ActionProps<T> extends _Action {
    type: T;
}

export class Action<Type, Props extends ActionProps<Type>> {

    public static get type(): string {
        return Reflect.getMetadata(ActionTypeMetadataKey, this);
    }

    constructor(...args: any[]) {
        // any-args constructor so typeof keyword works
    }

    public get type(): Type {
        return (this.constructor as any).type;
    }

    public serialize(): Props {
        if (!this.type) throw new Error(`Can't serialize action without a type.`);

        const serialized: Props = {type: this.type} as Props;

        Object.getOwnPropertyNames(this)
            .forEach(name => serialized[name] = this[name]);

        return serialized;
    }

}

export default Action;