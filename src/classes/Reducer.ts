import {Action, Reducer as _Reducer} from "redux";
import "reflect-metadata";

export interface ActionReducer {
    methodName: string;
    actionTypes: string[];
}

export abstract class Reducer<State> {

    [name: string]: any;

    public static createReducer<State>(): _Reducer<State> {
        return new (this as any)().reduce;
    }

    protected readonly reducers: ActionReducer[] = Reflect.getMetadata(`Reducer.reducers`, this.constructor.prototype, `reducers`);

    constructor(...args: any[]);
    constructor() {
        this.reduce = this.reduce.bind(this);
    }

    protected abstract getInitialState(): State;

    public reduce(state: State, action: Action): State {
        state = state == null ? this.getInitialState() : state

        for (const reducer of this.reducers) {
            if (reducer.actionTypes.indexOf(action.type) !== -1) {
                return this[reducer.methodName](state, action);
            }
        }

        return state;
    }

}

export default Reducer;