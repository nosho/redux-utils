import {
    Action as _Action,
    ActionCreatorsMapObject,
    bindActionCreators,
    Dispatch,
    Middleware,
    MiddlewareAPI
} from "redux";
import thunk from "redux-thunk";
import {Action} from "./Action";

// TODO refactor to separate node module
export abstract class ActionCreator<ActionCreators> {

    [name: string]: any;

    public static actionClassesMiddleware: Middleware = <S extends _Action>(store: MiddlewareAPI<S>) => (next: Dispatch<S>) => (action: S): S => {
        return next(action instanceof Action ? (action as Action<any, S>).serialize() : action)
    };

    public static middleware: Middleware[] = [
        thunk,
        ActionCreator.actionClassesMiddleware
    ];

    public static bindToDispatch<TActionCreator extends ActionCreator<any>>(dispatch: Dispatch<any>): TActionCreator {
        return bindActionCreators<ActionCreatorsMapObject, TActionCreator>(
            new (this as any)(dispatch).serialize(),
            dispatch
        );
    }

    protected dispatch: Dispatch<any>;

    constructor(dispatch: Dispatch<any>) {
        this.dispatch = dispatch;
    }

    public serialize(): ActionCreatorsMapObject {
        const actionCreators: ActionCreatorsMapObject = {} as ActionCreatorsMapObject;

        Object.keys(this.constructor.prototype)
            .forEach(key => typeof this[key] === "function"
                ? actionCreators[key] = (...args: any[]) => (dispatch) => this[key](...args)
                : null
            );

        return actionCreators;
    }

}

export default ActionCreator;